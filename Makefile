##
## EPITECH PROJECT, 2018
## makefile
## File description:
## makfile
##

NAME    	=   philo

SRC     	=   $(SRCDIR)main.c	\
			$(SRCDIR)actions.c

SRCDIR  	=   src/

INC     	=   -I./inc

OBJ     	=   $(SRC:.c=.o)

CFLAGS      	=   -W -Wall -Werror -Wextra -L. -lriceferee -lpthread $(INC) -g

all:		$(NAME)

$(NAME):	$(OBJ)
		gcc $(OBJ) -o $(NAME) $(CFLAGS)
clean:
		rm -rf $(OBJ)

fclean:		clean
		rm -rf $(NAME)

re:		fclean all
