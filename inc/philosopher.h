/*
** EPITECH PROJECT, 2022
** Philosopher
** File description:
** Created by hugo,
*/

#ifndef PHILOSOPHER_PHILOSOPHER_H
# define PHILOSOPHER_PHILOSOPHER_H

#include <stdlib.h>
#include <memory.h>
#include <stdio.h>
#include <zconf.h>
#include "extern.h"

typedef enum e_pstatus {
	REST,
	EAT,
	THINK,
	UNDEFINED
} e_pstatus;

typedef struct s_philo {

	pthread_mutex_t m_stick_lock;
	unsigned int to_eat;
	unsigned int nb_philo;
	unsigned int id;
	enum e_pstatus p_status;
	unsigned int time_eaten;
	struct s_philo *next;
	int bool;
} t_philo;

void philo_sleep(t_philo *philo);
void philo_eat(t_philo *philo);
void philo_think(t_philo *philo);
void print_err(t_philo *philo, int err);

#endif //PHILOSOPHER_PHILOSOPHER_H
