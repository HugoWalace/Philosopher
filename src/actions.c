/*
** EPITECH PROJECT, 2018
** philosopher
** File description:
** actions
*/

#include "../inc/philosopher.h"

void philo_sleep(t_philo *philo)
{
	philo->p_status = REST;
	lphilo_sleep();
}

void philo_eat(t_philo *philo)
{
	philo->p_status = EAT;
	philo->time_eaten++;
	lphilo_take_chopstick(&philo->m_stick_lock);
	lphilo_take_chopstick(&philo->next->m_stick_lock);
	lphilo_eat();
	usleep(500);
	lphilo_release_chopstick(&philo->m_stick_lock);
        pthread_mutex_unlock(&philo->m_stick_lock);
	lphilo_release_chopstick(&philo->next->m_stick_lock);
	pthread_mutex_unlock(&philo->next->m_stick_lock);
}

void philo_think(t_philo *philo)
{
	philo->p_status = THINK;
	lphilo_take_chopstick(&philo->m_stick_lock);
	lphilo_think();
	usleep(200);
	lphilo_release_chopstick(&philo->m_stick_lock);
	pthread_mutex_unlock(&philo->m_stick_lock);
}
