/*
** EPITECH PROJECT, 2022
** Philosopher
** File description:
** Created by hugo,
*/

#include <stdio.h>
#include "philosopher.h"

int check_arg(int ac, char **av, int *nb_p, int *nb_e)
{
	if (ac != 5)
		return (EXIT_FAILURE);
	if (strcmp(av[1], "-p") != 0)
		return (EXIT_FAILURE);
	if (strcmp(av[3], "-e") != 0)
		return (EXIT_FAILURE);
	*nb_e = atoi(av[4]);
	*nb_p = atoi(av[2]);
	return (EXIT_SUCCESS);
}

void *dining_table(void *arg)
{
	t_philo *philo = arg;

	while (philo->time_eaten < philo->to_eat) {
		pthread_mutex_trylock(&philo->m_stick_lock);
		philo_think(philo);
		pthread_mutex_trylock(&philo->m_stick_lock);
		pthread_mutex_trylock(&philo->next->m_stick_lock);
		philo_eat(philo);
		philo_sleep(philo);
	}
	return (NULL);
}

t_philo *initnode(int i, int e, int p)
{
	t_philo *nnode;

	nnode = malloc(sizeof(t_philo));
	if (nnode == NULL)
		return NULL;
	nnode->p_status = UNDEFINED;
	nnode->id = (unsigned)i;
	nnode->time_eaten = 0;
	nnode->to_eat = (unsigned)e;
	nnode->nb_philo = (unsigned)p;
	nnode->bool = 0;
	pthread_mutex_init(&nnode->m_stick_lock, NULL);
	nnode->next = NULL;
	return (nnode);
}

int thread(int nb_p, int nb_e)
{
	pthread_t *thread;
	t_philo *philo = initnode(0, nb_e, nb_p);
	t_philo *tmp = philo;
	int i = 0;

	thread = malloc(sizeof(pthread_t) * (nb_p + 1));
	for (int x = 1; x < nb_p; ++x) {
		tmp->next = initnode(x, nb_e, nb_p);
		tmp = tmp->next;
	}
	tmp->next = philo;
	while (philo->bool != 1) {
		pthread_create(&(thread[i]), NULL, dining_table, philo);
		philo->bool = 1;
		philo = philo->next;
		i++;
	}
	for (int j = 0; j < nb_p; ++j)
		pthread_join(thread[j], NULL);
	return (EXIT_SUCCESS);
}

int main(int ac, char **av)
{
	int nb_p;
	int nb_e;

	if (ac == 2 && strcmp(av[1], "--help") == 0) {
		printf("USAGE\n\t./philo -p nbr_p -e nbr_e\n");
		printf("DESCRIPTION\n\t-p nbr_p number of philosophers\n");
		printf("\t-e nbr_e maximum number times a philosopher eats ");
		printf("before exiting the program\n");
		return (EXIT_SUCCESS);
	}
	if (check_arg(ac, av, &nb_p, &nb_e) != EXIT_SUCCESS)
		return (EXIT_FAILURE);
	RCFStartup(ac, av);
	thread(nb_p, nb_e);
	RCFCleanup();
	return (EXIT_SUCCESS);
}
